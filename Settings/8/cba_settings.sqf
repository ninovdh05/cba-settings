// ACE Advanced Ballistics
force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force ace_advanced_ballistics_bulletTraceEnabled = true;
force ace_advanced_ballistics_enabled = false;
force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Throwing
ace_advanced_throwing_enabled = true;
force ace_advanced_throwing_enablePickUp = true;
force ace_advanced_throwing_enablePickUpAttached = true;
ace_advanced_throwing_showMouseControls = true;
ace_advanced_throwing_showThrowArc = true;

// ACE Advanced Vehicle Damage
force force ace_vehicle_damage_enableCarDamage = false;
force force ace_vehicle_damage_enabled = false;
force force ace_vehicle_damage_removeAmmoDuringCookoff = false;

// ACE Arsenal
force ace_arsenal_allowDefaultLoadouts = true;
force ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
force ace_arsenal_enableIdentityTabs = true;
ace_arsenal_enableModIcons = true;
ace_arsenal_EnableRPTLog = false;
ace_arsenal_fontHeight = 4.5;
ace_arsenal_loadoutsSaveFace = false;
ace_arsenal_loadoutsSaveInsignia = true;
ace_arsenal_loadoutsSaveVoice = false;

// ACE Artillery
force ace_artillerytables_advancedCorrections = false;
force ace_artillerytables_disableArtilleryComputer = false;
force ace_mk6mortar_airResistanceEnabled = false;
force ace_mk6mortar_allowCompass = true;
force ace_mk6mortar_allowComputerRangefinder = true;
force ace_mk6mortar_useAmmoHandling = false;

// ACE Captives
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 0;
force ace_captives_requireSurrenderAi = false;

// ACE Casings
ace_casings_enabled = true;
ace_casings_maxCasings = 250;

// ACE Common
force force ace_common_allowFadeMusic = true;
force force ace_common_checkPBOsAction = 2;
force force ace_common_checkPBOsCheckAll = true;
force force ace_common_checkPBOsWhitelist = "[""3denEnhanced"",""CHTR_TFAR_Setter"",""MIRA_Vehicle_Medical"",""UPSL_aime"",""UPSL_aime_uav_terminal"",""UPSL_aime_vehicle_controls"",""UPSL_aime_vehicle_seats"",""UPSL_aime_change_ammo"",""UPSL_aime_group"",""UPSL_aime_inventory"",""athena"",""DNI_ZeusFPSMonitor"",""Radio_Animations"",""GF_ReColor"",""DIS_Enhanced_Gps"",""DIS_enhanced_map_ace"",""A3TI"",""3rdView"",""WP_Mod"",""CHVD"",""viewDistance_TAW"",""DS_OBE"",""DS_OBE_Layer"",""Vile_HUD"",""L_Suppress_Suppress_main"",""UTGX_Compass"",""ReducedHazeMod"",""real_fix_3rd_person_view"",""dwyl_main"",""rwyl_main"",""ZEI"",""DEVAS_TacticalReady"",""PH_TacReady"",""kka3_gestures"",""kka3_gestures_ace"",""UtesLowGrass"",""LowGrassChernarus"",""LowGrassChernarus_Summer"",""mrb_a3_seavesselvisibility"",""LowGrassBootcamp_acr"",""VANA"",""mrb_a3_vehiclevisibility"",""mrb_a3_airvisibility"",""ANZ_NoGrassMod"",""gcam"",""CAU_UserInputMenus"",""CAU_colorPicker"",""CW_9LINERS_AND_NOTEPAD"",""bettinv_main"",""bettinv_main_ace"",""ASAAYU_ACE_MEDICAL_ASSISTANT"",""cba_settings_userconfig""]";
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [1,1,1,1];
ace_common_epilepsyFriendlyMode = false;
ace_common_progressBarInfo = 2;
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 0;

// ACE Cook off
force ace_cookoff_ammoCookoffDuration = 1;
force ace_cookoff_destroyVehicleAfterCookoff = false;
force force ace_cookoff_enable = 0;
force ace_cookoff_enableAmmobox = false;
force ace_cookoff_enableAmmoCookoff = false;
force ace_cookoff_enableFire = false;
force ace_cookoff_probabilityCoef = 1;

// ACE Crew Served Weapons
force ace_csw_ammoHandling = 2;
force ace_csw_defaultAssemblyMode = false;
ace_csw_dragAfterDeploy = false;
force ace_csw_handleExtraMagazines = true;
force ace_csw_handleExtraMagazinesType = 0;
force ace_csw_progressBarTimeCoefficent = 1;

// ACE Dragging
ace_dragging_dragAndFire = true;

// ACE Explosives
ace_explosives_customTimerDefault = 30;
force ace_explosives_customTimerMax = 900;
force ace_explosives_customTimerMin = 5;
force ace_explosives_explodeOnDefuse = false;
force ace_explosives_punishNonSpecialists = false;
force ace_explosives_requireSpecialist = false;

// ACE Field Rations
force acex_field_rations_affectAdvancedFatigue = false;
force force acex_field_rations_enabled = false;
acex_field_rations_hudShowLevel = 0;
acex_field_rations_hudTransparency = -1;
acex_field_rations_hudType = 0;
force acex_field_rations_hungerSatiated = 1;
force acex_field_rations_terrainObjectActions = true;
force acex_field_rations_thirstQuenched = 1;
force acex_field_rations_timeWithoutFood = 2;
force acex_field_rations_timeWithoutWater = 2;
force acex_field_rations_waterSourceActions = 2;

// ACE Fire
force ace_fire_dropWeapon = 1;
force ace_fire_enabled = true;
force ace_fire_enableFlare = false;
ace_fire_enableScreams = true;

// ACE Fortify
force force ace_fortify_markObjectsOnMap = 1;
force force ace_fortify_timeCostCoefficient = 1;
force force ace_fortify_timeMin = 1.5;
force force acex_fortify_settingHint = 1;

// ACE Fragmentation Simulation
force force ace_frag_enabled = false;
force force ace_frag_maxTrack = 10;
force force ace_frag_maxTrackPerFrame = 10;
force force ace_frag_reflectionsEnabled = false;
force force ace_frag_spallEnabled = false;

// ACE G-Forces
force ace_gforces_coef = 0.40031;
force ace_gforces_enabledFor = 0;

// ACE Goggles
ace_goggles_effects = 2;
ace_goggles_showClearGlasses = false;
ace_goggles_showInThirdPerson = false;

// ACE Grenades
force ace_grenades_convertExplosives = true;

// ACE Headless
force acex_headless_delay = 15;
force acex_headless_enabled = false;
force acex_headless_endMission = 0;
force acex_headless_log = false;
force acex_headless_transferLoadout = 0;

// ACE Hearing
force ace_hearing_autoAddEarplugsToUnits = true;
ace_hearing_disableEarRinging = true;
force ace_hearing_earplugsVolume = 0.5;
force ace_hearing_enableCombatDeafness = false;
force ace_hearing_enabledForZeusUnits = true;
force ace_hearing_unconsciousnessVolume = 0.647813;

// ACE Interaction
force ace_interaction_disableNegativeRating = true;
force ace_interaction_enableGroupRenaming = true;
ace_interaction_enableMagazinePassing = true;
force ace_interaction_enableTeamManagement = true;
ace_interaction_enableWeaponAttachments = true;
force ace_interaction_interactWithTerrainObjects = false;

// ACE Interaction Menu
ace_gestures_showOnInteractionMenu = 2;
ace_interact_menu_actionOnKeyRelease = true;
ace_interact_menu_addBuildingActions = false;
ace_interact_menu_alwaysUseCursorInteraction = false;
ace_interact_menu_alwaysUseCursorSelfInteraction = true;
ace_interact_menu_colorShadowMax = [0,0,0,1];
ace_interact_menu_colorShadowMin = [0,0,0,0.25];
ace_interact_menu_colorTextMax = [1,1,1,1];
ace_interact_menu_colorTextMin = [1,1,1,0.25];
ace_interact_menu_consolidateSingleChild = false;
ace_interact_menu_cursorKeepCentered = false;
ace_interact_menu_cursorKeepCenteredSelfInteraction = false;
ace_interact_menu_menuAnimationSpeed = 0;
ace_interact_menu_menuBackground = 0;
ace_interact_menu_menuBackgroundSelf = 0;
ace_interact_menu_selectorColor = [1,0,0];
ace_interact_menu_shadowSetting = 2;
ace_interact_menu_textSize = 2;
ace_interact_menu_useListMenu = false;
ace_interact_menu_useListMenuSelf = true;

// ACE Interaction Menu (Self) - More
ace_interact_menu_more__ACE_CheckAirTemperature = false;
ace_interact_menu_more__ace_csw_deploy = false;
ace_interact_menu_more__ACE_Equipment = false;
ace_interact_menu_more__ACE_Explosives = false;
ace_interact_menu_more__ace_field_rations = false;
ace_interact_menu_more__ace_fortify = false;
ace_interact_menu_more__ace_gestures = false;
ace_interact_menu_more__ace_intelitems = false;
ace_interact_menu_more__ACE_MapFlashlight = false;
ace_interact_menu_more__ACE_MapGpsHide = false;
ace_interact_menu_more__ACE_MapGpsShow = false;
ace_interact_menu_more__ACE_MapTools = false;
ace_interact_menu_more__ACE_Medical = false;
ace_interact_menu_more__ACE_Medical_Menu = false;
ace_interact_menu_more__ACE_MoveRallypoint = false;
ace_interact_menu_more__ACE_RepackMagazines = false;
ace_interact_menu_more__ace_sandbag_place = false;
ace_interact_menu_more__ACE_StartSurrenderingSelf = false;
ace_interact_menu_more__ACE_StopEscortingSelf = false;
ace_interact_menu_more__ACE_StopSurrenderingSelf = false;
ace_interact_menu_more__ACE_Tags = false;
ace_interact_menu_more__ACE_TeamManagement = false;
ace_interact_menu_more__ace_zeus_create = false;
ace_interact_menu_more__ace_zeus_delete = false;
ace_interact_menu_more__acex_sitting_Stand = false;
ace_interact_menu_more__kka3_anim = false;
ace_interact_menu_more__Medical = false;
ace_interact_menu_more__TFAR_Radio = false;
ace_interact_menu_more__UPSL_aime_change_ammo_ammo_class = false;
ace_interact_menu_more__UPSL_aime_inventory_assemble_action = false;
ace_interact_menu_more__UPSL_aime_uav_terminal_uav_action = false;
ace_interact_menu_more__UPSL_aime_vehicle_controls_user_actions = false;
ace_interact_menu_more__UPSL_aime_vehicle_seats_change_action = false;
ace_interact_menu_more__UPSL_aime_vehicle_seats_eject_action = false;
ace_interact_menu_more__UPSL_aime_vehicle_seats_getout_action = false;
ace_interact_menu_more__UPSL_aime_vehicle_seats_turnin_action = false;

// ACE Interaction Menu (Self) - Move to Root
ace_interact_menu_moveToRoot__ACE_Equipment__ace_atragmx_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_attach_Attach = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_attach_Detach = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_CheckDogtags = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_Chemlights = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_dagr_menu = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_dagr_menu__ace_dagr_toggle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_goggles_wipeGlasses = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_status = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponOff = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponSwap = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponTo = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_huntir_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_interaction_weaponAttachments = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open__ace_kestrel4500_hide = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open__ace_kestrel4500_show = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_marker_flags = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure__ace_microdagr_close = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure__ace_microdagr_show = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_activate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_connectHeadphones = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_deactivate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_disconnectHeadphones = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_mk6mortar_rangetable = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CheckTemperature = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CheckTemperatureSpareBarrels = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CoolWeaponWithItem = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_SwapBarrel = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_UnJam = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_PutInEarplugs = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open__ace_rangecard_makeCopy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open__ace_rangecard_openCopy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_reload_checkAmmo = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_RemoveEarplugs = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_scopes_adjustZero = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_scopes_resetZero = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_spottingscope_place = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_TacticalLadders = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__ace_trenches_digEnvelopeBig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__ace_trenches_digEnvelopeSmall = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeGiant = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeLongNameEmplacment = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeShort = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeVehicle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_tripod_place = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ADDON_EXCEPT = false;
ace_interact_menu_moveToRoot__ACE_Equipment__addSound = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Load_Both = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Load_Root = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Load_Root__Load_Both = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Load_Root__Load_LR = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Load_Root__Load_SR = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Load_Root__Load_VLR = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__LR_Root = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__LR_Root__LR_Load = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__LR_Root__LR_Save = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Save_Root = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Save_Root__Save_LR = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Save_Root__Save_SR = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__Save_Root__Save_VLR = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__SR_Root = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__SR_Root__SR_Load = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__SR_Root__SR_Save = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__VLR_Root = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__VLR_Root__LR_Load = false;
ace_interact_menu_moveToRoot__ACE_Equipment__CHTR_TFAR_Setter__VLR_Root__LR_Save = false;
ace_interact_menu_moveToRoot__ACE_Equipment__fow_mortars_c_openRangeTable_m2 = false;
ace_interact_menu_moveToRoot__ACE_Equipment__fow_mortars_c_openRangeTable_type97 = false;
ace_interact_menu_moveToRoot__ACE_Equipment__immersion_pops_start_cig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__immersion_pops_stop_cig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__immersion_pops_take_cig_from_pack = false;
ace_interact_menu_moveToRoot__ACE_Equipment__murshun_cigs_start_cig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__murshun_cigs_stop_cig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__murshun_cigs_take_cig_from_pack = false;
ace_interact_menu_moveToRoot__ACE_Equipment__placeAED = false;
ace_interact_menu_moveToRoot__ACE_Equipment__removeSound = false;
ace_interact_menu_moveToRoot__ACE_Equipment__UPSL_aime_uav_terminal_gps_action = false;
ace_interact_menu_moveToRoot__ACE_Equipment__UPSL_aime_uav_terminal_term_action = false;
ace_interact_menu_moveToRoot__ACE_Equipment__UPSL_aime_uav_terminal_uav_action = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_onBack = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_onChest = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_swap = false;
ace_interact_menu_moveToRoot__ACE_Explosives__ACE_Cellphone = false;
ace_interact_menu_moveToRoot__ACE_Explosives__ACE_Place = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Advance = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_CeaseFire = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Cover = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Engage = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Follow = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Forward = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Freeze = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Go = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Hold = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Point = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Regroup = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Stop = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Up = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Warning = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlignCompass = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlignNorth = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsHide = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsShowNormal = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsShowSmall = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_ArmLeft = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_ArmRight = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Head = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_LegLeft = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_LegRight = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Torso = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Torso__TriageCard = false;
ace_interact_menu_moveToRoot__ACE_Medical__dev_enzymeCapsule = false;
ace_interact_menu_moveToRoot__ACE_Medical__dev_enzymeCapsule_refined = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_BecomeLeader = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamBlue = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamGreen = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamRed = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamYellow = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_LeaveGroup = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_LeaveTeam = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_RenameGroup = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__diwako_dui_buddy_buddy_action_team_remove = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__UPSL_aime_group_drop_leader_action = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_direonepeace = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_direonerock = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Facepalm = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_kata = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_pissing = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Pushup = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Threaten = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Thumbsup = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_cancel = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_crouch = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_crouch_thumbup = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_EmergencyStop = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_EnginesOff = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_EnginesOn = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Launch = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Left = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Right = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Slow = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Stop = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Straight = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_Crazy_Dance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_crazydrunkdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_dubstepdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_dubstepPop = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_DuoIvan = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_hiphopdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_nightclubdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_robotdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_russiandance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2__kka3_direone1b = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2__kka3_direone2b = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2__kka3_direone3b = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_1 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_2 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_3 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_4 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_5 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_6 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_7 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Angry = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Default = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Sad = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Smile = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_scared = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose1 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose10 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose2 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose3 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose4 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose5 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose6 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose7 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose8 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose9 = false;
ace_interact_menu_moveToRoot__Medical__ACE_Head = false;
ace_interact_menu_moveToRoot__Medical__ACE_Head__CheckBloodPressure = false;
ace_interact_menu_moveToRoot__UPSL_aime_vehicle_seats_change_action__UPSL_aime_vehicle_seats_turnout_action = false;
ace_interact_menu_moveToRoot__UPSL_aime_vehicle_seats_eject_action__UPSL_aime_vehicle_seats_eject_confirm_action = false;
ace_interact_menu_moveToRoot__UPSL_aime_vehicle_seats_getout_action__UPSL_aime_vehicle_seats_eject_action = false;

// ACE Logistics
ace_cargo_carryAfterUnload = true;
force ace_cargo_enable = true;
ace_cargo_enableRename = true;
force ace_cargo_loadTimeCoefficient = 5;
ace_cargo_openAfterUnload = 0;
force ace_cargo_paradropTimeCoefficent = 2.5;
force ace_rearm_distance = 20;
force ace_rearm_level = 1;
force ace_rearm_supply = 0;
force ace_refuel_hoseLength = 30;
force ace_refuel_progressDuration = 2;
force ace_refuel_rate = 3;
force ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = true;
force ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
force ace_repair_engineerSetting_fullRepair = 0;
force ace_repair_engineerSetting_repair = 0;
force ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 3;
force ace_repair_fullRepairRequiredItems = ["ace_repair_anyToolKit"];
force ace_repair_locationsBoostTraining = false;
force ace_repair_miscRepairRequiredItems = ["ace_repair_anyToolKit"];
force ace_repair_repairDamageThreshold = 0.6;
force ace_repair_repairDamageThreshold_engineer = 0.4;
force ace_repair_wheelRepairRequiredItems = [];
force ace_towing_addRopeToVehicleInventory = true;

// ACE Magazine Repack
ace_magazinerepack_repackAnimation = true;
ace_magazinerepack_repackLoadedMagazines = true;
force ace_magazinerepack_timePerAmmo = 1.5;
force ace_magazinerepack_timePerBeltLink = 8;
force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force ace_map_BFT_Enabled = false;
force ace_map_BFT_HideAiGroups = false;
force ace_map_BFT_Interval = 1;
force ace_map_BFT_ShowPlayerNames = false;
force ace_map_DefaultChannel = 0;
force ace_map_mapGlow = true;
force ace_map_mapIllumination = true;
force ace_map_mapLimitZoom = false;
force ace_map_mapShake = true;
force ace_map_mapShowCursorCoordinates = false;
force force ace_markers_moveRestriction = 0;
ace_markers_timestampEnabled = true;
ace_markers_timestampFormat = "HH:MM";
ace_markers_timestampHourFormat = 24;

// ACE Map Gestures
ace_map_gestures_allowCurator = true;
ace_map_gestures_allowSpectator = true;
ace_map_gestures_briefingMode = 0;
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force ace_map_gestures_enabled = true;
force ace_map_gestures_interval = 0.03;
force ace_map_gestures_maxRange = 7;
force ace_map_gestures_maxRangeCamera = 14;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];
force ace_map_gestures_onlyShowFriendlys = false;

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
force force ace_medical_ai_enabledFor = 2;
force force ace_medical_AIDamageThreshold = 1;
force force ace_medical_bleedingCoefficient = 0.5;
force force ace_medical_blood_bloodLifetime = 300;
force force ace_medical_blood_enabledFor = 2;
force force ace_medical_blood_maxBloodObjects = 500;
force force ace_medical_deathChance = 0.05;
force force ace_medical_enableVehicleCrashes = true;
force force ace_medical_fatalDamageSource = 1;
ace_medical_feedback_bloodVolumeEffectType = 0;
ace_medical_feedback_enableHUDIndicators = true;
ace_medical_feedback_painEffectType = 0;
force force ace_medical_fractureChance = 0.5;
force force ace_medical_fractures = 1;
ace_medical_gui_bloodLossColor_0 = [1,1,1,1];
ace_medical_gui_bloodLossColor_1 = [1,0.95,0.64,1];
ace_medical_gui_bloodLossColor_2 = [1,0.87,0.46,1];
ace_medical_gui_bloodLossColor_3 = [1,0.8,0.33,1];
ace_medical_gui_bloodLossColor_4 = [1,0.72,0.24,1];
ace_medical_gui_bloodLossColor_5 = [1,0.63,0.15,1];
ace_medical_gui_bloodLossColor_6 = [1,0.54,0.08,1];
ace_medical_gui_bloodLossColor_7 = [1,0.43,0.02,1];
ace_medical_gui_bloodLossColor_8 = [1,0.3,0,1];
ace_medical_gui_bloodLossColor_9 = [1,0,0,1];
ace_medical_gui_damageColor_0 = [1,1,1,1];
ace_medical_gui_damageColor_1 = [0.75,0.95,1,1];
ace_medical_gui_damageColor_2 = [0.62,0.86,1,1];
ace_medical_gui_damageColor_3 = [0.54,0.77,1,1];
ace_medical_gui_damageColor_4 = [0.48,0.67,1,1];
ace_medical_gui_damageColor_5 = [0.42,0.57,1,1];
ace_medical_gui_damageColor_6 = [0.37,0.47,1,1];
ace_medical_gui_damageColor_7 = [0.31,0.36,1,1];
ace_medical_gui_damageColor_8 = [0.22,0.23,1,1];
ace_medical_gui_damageColor_9 = [0,0,1,1];
ace_medical_gui_enableActions = 0;
ace_medical_gui_enableMedicalMenu = 1;
ace_medical_gui_enableSelfActions = true;
ace_medical_gui_interactionMenuShowTriage = 1;
force force ace_medical_gui_maxDistance = 3;
ace_medical_gui_openAfterTreatment = true;
force force ace_medical_gui_showBloodlossEntry = true;
force force ace_medical_ivFlowRate = 1.5;
force force ace_medical_limping = 1;
force force ace_medical_painCoefficient = 1;
force force ace_medical_painUnconsciousChance = 0.4;
force force ace_medical_playerDamageThreshold = 1.5;
force force ace_medical_spontaneousWakeUpChance = 0.751836;
force force ace_medical_spontaneousWakeUpEpinephrineBoost = 20;
force force ace_medical_statemachine_AIUnconsciousness = true;
force force ace_medical_statemachine_cardiacArrestBleedoutEnabled = true;
force force ace_medical_statemachine_cardiacArrestTime = 300;
force force ace_medical_statemachine_fatalInjuriesAI = 0;
force force ace_medical_statemachine_fatalInjuriesPlayer = 0;
force force ace_medical_treatment_advancedBandages = 2;
force force ace_medical_treatment_advancedDiagnose = 1;
force force ace_medical_treatment_advancedMedication = true;
force force ace_medical_treatment_allowBodyBagUnconscious = false;
force force ace_medical_treatment_allowLitterCreation = true;
force force ace_medical_treatment_allowSelfIV = 1;
force force ace_medical_treatment_allowSelfPAK = 0;
force force ace_medical_treatment_allowSelfStitch = 0;
force force ace_medical_treatment_allowSharedEquipment = 0;
force force ace_medical_treatment_clearTrauma = 1;
force force ace_medical_treatment_consumePAK = 0;
force force ace_medical_treatment_consumeSurgicalKit = 0;
force force ace_medical_treatment_convertItems = 0;
force force ace_medical_treatment_cprSuccessChanceMax = 0.4;
force force ace_medical_treatment_cprSuccessChanceMin = 0.4;
force force ace_medical_treatment_holsterRequired = 0;
force force ace_medical_treatment_litterCleanupDelay = 300;
force force ace_medical_treatment_locationEpinephrine = 0;
force force ace_medical_treatment_locationIV = 0;
force force ace_medical_treatment_locationPAK = 0;
force force ace_medical_treatment_locationsBoostTraining = true;
force force ace_medical_treatment_locationSurgicalKit = 0;
force force ace_medical_treatment_maxLitterObjects = 500;
force force ace_medical_treatment_medicEpinephrine = 0;
force force ace_medical_treatment_medicIV = 1;
force force ace_medical_treatment_medicPAK = 1;
force force ace_medical_treatment_medicSurgicalKit = 1;
force force ace_medical_treatment_timeCoefficientPAK = 1;
force force ace_medical_treatment_treatmentTimeAutoinjector = 3;
force force ace_medical_treatment_treatmentTimeBodyBag = 15;
force force ace_medical_treatment_treatmentTimeCPR = 10;
force force ace_medical_treatment_treatmentTimeIV = 10;
force force ace_medical_treatment_treatmentTimeSplint = 5;
force force ace_medical_treatment_treatmentTimeTourniquet = 3;
force force ace_medical_treatment_woundReopenChance = 1;
force force ace_medical_treatment_woundStitchTime = 2;

// ACE Name Tags
force ace_nametags_ambientBrightnessAffectViewDist = 1;
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
ace_nametags_nametagColorMain = [1,1,1,1];
ace_nametags_nametagColorRed = [1,0.67,0.67,1];
ace_nametags_nametagColorYellow = [1,1,0.67,1];
force ace_nametags_playerNamesMaxAlpha = 0.8;
force ace_nametags_playerNamesViewDistance = 5;
force ace_nametags_showCursorTagForVehicles = false;
ace_nametags_showNamesForAI = false;
ace_nametags_showPlayerNames = 1;
ace_nametags_showPlayerRanks = true;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 2;

// ACE Nightvision
force ace_nightvision_aimDownSightsBlur = 0;
force ace_nightvision_disableNVGsWithSights = false;
force ace_nightvision_effectScaling = 0;
force ace_nightvision_fogScaling = 0;
force ace_nightvision_noiseScaling = 0;
ace_nightvision_shutterEffects = true;

// ACE Overheating
force ace_overheating_cookoffCoef = 1;
force ace_overheating_coolingCoef = 1;
ace_overheating_displayTextOnJam = true;
force ace_overheating_enabled = false;
force ace_overheating_heatCoef = 1;
force ace_overheating_jamChanceCoef = 1;
force ace_overheating_overheatingDispersion = true;
force ace_overheating_overheatingRateOfFire = true;
ace_overheating_particleEffectsAndDispersionDistance = 3000;
ace_overheating_showParticleEffects = false;
ace_overheating_showParticleEffectsForEveryone = false;
force ace_overheating_suppressorCoef = 1;
force ace_overheating_unJamFailChance = 0.1;
force ace_overheating_unJamOnreload = false;
force ace_overheating_unJamOnSwapBarrel = false;

// ACE Pharmacy - AED
force force aceP_circulation_CPR_Chance_Default = 60;
force force aceP_circulation_CPR_Chance_Doctor = 70;
force force aceP_circulation_CPR_Chance_RegularMedic = 60;
force force aceP_circulation_DeactMon_whileAED_X = true;
force force aceP_circulation_distanceLimit_AEDX = 30;
force force aceP_circulation_enable_CPR_Chances = true;
force force aceP_circulation_medLvl_AED_X = 2;
force force aceP_circulation_SuccesCh_AED = 80;
force force aceP_circulation_SuccesCh_AED_X = 90;
force force aceP_circulation_timeLimit_AEDX = 1800;
force force aceP_circulation_useLocation_AED = 0;

// ACE Pharmacy - Fractures
force force aceP_circulation_closedLocation = 0;
force force aceP_circulation_closedReduction = 2;
force force aceP_circulation_closedTime = 10;
force force aceP_circulation_compoundChance = 30;
force force aceP_circulation_enable_fracture = false;
force force aceP_circulation_etomidateTime = 45;
force force aceP_circulation_fractureCheck_Level = 0;
force force aceP_circulation_incisionTime = 10;
force force aceP_circulation_openTime = 15;
force force aceP_circulation_simpleChance = 60;
force force aceP_circulation_surgicalAction = 2;
force force aceP_circulation_surgicalLocation = 2;
force force aceP_circulation_surgicalTime = 8;

// ACE Pharmacy - Medications
force aceP_circulation_aiEnableAdvanced = false;
force force aceP_circulation_blockChance = 0;
force aceP_circulation_coagulation = true;
force aceP_circulation_factorDrugs = 2;
force force aceP_circulation_IOestablish = 3;
force force aceP_circulation_IVdrop = 1200;
force force aceP_circulation_IVdropEnable = false;
force force aceP_circulation_IVestablish = 2;
force force aceP_circulation_IVmedic = 2;
force force aceP_circulation_IVreuse = false;
force force aceP_circulation_kidneyAction = false;
force force aceP_circulation_maxStack = 5;
force force aceP_circulation_PushTime = 5;
force aceP_circulation_surgicalDrugs = 2;
force aceP_circulation_vasoDrugs = 1;

// ACE Pointing
force ace_finger_enabled = false;
ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 20;
force ace_finger_proximityScaling = false;
force ace_finger_sizeCoef = 1;

// ACE Pylons
force ace_pylons_enabledForZeus = true;
force ace_pylons_enabledFromAmmoTrucks = true;
force ace_pylons_rearmNewPylons = true;
force ace_pylons_requireEngineer = false;
force ace_pylons_requireToolkit = true;
force ace_pylons_searchDistance = 15;
force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force ace_quickmount_distance = 5;
force ace_quickmount_enabled = true;
ace_quickmount_enableMenu = 3;
ace_quickmount_priority = 3;
force ace_quickmount_speed = 20;

// ACE Respawn
force force ace_respawn_removeDeadBodiesDisconnected = false;
force ace_respawn_savePreDeathGear = false;

// ACE Scopes
force ace_scopes_correctZeroing = false;
force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force ace_scopes_defaultZeroRange = 100;
force force ace_scopes_enabled = false;
force ace_scopes_forceUseOfAdjustmentTurrets = false;
force ace_scopes_overwriteZeroRange = false;
force ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force ace_scopes_zeroReferenceHumidity = 0;
force ace_scopes_zeroReferenceTemperature = 15;

// ACE Sitting
force acex_sitting_enable = false;

// ACE Spectator
force ace_spectator_enableAI = false;
ace_spectator_maxFollowDistance = 5;
force ace_spectator_restrictModes = 0;
force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force ace_switchunits_enableSafeZone = false;
force ace_switchunits_enableSwitchUnits = false;
force ace_switchunits_safeZoneRadius = 0;
force ace_switchunits_switchToCivilian = false;
force ace_switchunits_switchToEast = false;
force ace_switchunits_switchToIndependent = false;
force ace_switchunits_switchToWest = false;

// ACE TFAR Radio Setter
CHTR_TFAR_Setter_Layout = false;
CHTR_TFAR_Setter_Shortcut = false;
CHTR_TFAR_Setter_ShowLR = true;
CHTR_TFAR_Setter_ShowSR = true;
CHTR_TFAR_Setter_ShowVLR = false;

// ACE Trenches
force ace_trenches_bigEnvelopeDigDuration = 25;
force ace_trenches_bigEnvelopeRemoveDuration = 15;
force ace_trenches_smallEnvelopeDigDuration = 20;
force ace_trenches_smallEnvelopeRemoveDuration = 12;

// ACE Uncategorized
force ace_fastroping_requireRopeItems = false;
force ace_gunbag_swapGunbagEnabled = true;
force ace_hitreactions_minDamageToTrigger = 0.1;
ace_inventory_inventoryDisplaySize = 0;
force ace_laser_dispersionCount = 2;
force ace_laser_showLaserOnMap = 2;
force ace_marker_flags_placeAnywhere = false;
force ace_microdagr_mapDataAvailable = 2;
force ace_microdagr_waypointPrecision = 3;
force ace_noradio_enabled = true;
ace_optionsmenu_showNewsOnMainMenu = true;
force ace_overpressure_distanceCoefficient = 1;
force ace_parachute_failureChance = 0;
ace_parachute_hideAltimeter = true;
ace_tagging_quickTag = 0;

// ACE User Interface
force ace_ui_allowSelectiveUI = true;
ace_ui_ammoCount = false;
ace_ui_ammoType = true;
ace_ui_commandMenu = true;
force ace_ui_enableSpeedIndicator = true;
ace_ui_firingMode = true;
ace_ui_groupBar = false;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_hideDefaultActionIcon = false;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
force ace_vehiclelock_defaultLockpickStrength = -1;
force ace_vehiclelock_lockVehicleInventory = false;
force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE Vehicle Medical
MIRA_Vehicle_Medical_CacheInterval = 0.4;
MIRA_Vehicle_Medical_EnableAVM = true;
MIRA_Vehicle_Medical_EnableIncapacitated = false;
MIRA_Vehicle_Medical_EnableStable = true;
MIRA_Vehicle_Medical_EnableUnstable = true;
MIRA_Vehicle_Medical_Incapacitated_CanUnloadAll = true;
MIRA_Vehicle_Medical_Incapacitated_ShowCount = false;
MIRA_Vehicle_Medical_Stable_ShowCount = true;
MIRA_Vehicle_Medical_Stable_ThresholdLowBP = 80;
MIRA_Vehicle_Medical_Stable_ThresholdLowHR = 50;
MIRA_Vehicle_Medical_Stable_TrackFractures = true;
MIRA_Vehicle_Medical_Stable_TrackIV = true;
MIRA_Vehicle_Medical_Stable_TrackLowBP = true;
MIRA_Vehicle_Medical_Stable_TrackLowHR = true;
MIRA_Vehicle_Medical_Stable_TrackNeedsBandage = true;
MIRA_Vehicle_Medical_Stable_TrackSplints = true;
MIRA_Vehicle_Medical_Stable_TrackStitchableWounds = true;
MIRA_Vehicle_Medical_Stable_TrackTourniquets = true;
MIRA_Vehicle_Medical_Unstable_DogtagsDeadOnly = true;
MIRA_Vehicle_Medical_Unstable_ShowCount = true;
MIRA_Vehicle_Medical_Unstable_TakeDogtags = true;
MIRA_Vehicle_Medical_Unstable_ThresholdLowBP = 80;
MIRA_Vehicle_Medical_Unstable_ThresholdLowHR = 50;
MIRA_Vehicle_Medical_Unstable_TrackBleeding = true;
MIRA_Vehicle_Medical_Unstable_TrackCardiacArrest = true;
MIRA_Vehicle_Medical_Unstable_TrackDead = true;
MIRA_Vehicle_Medical_Unstable_TrackIV = true;
MIRA_Vehicle_Medical_Unstable_TrackLegFractures = true;
MIRA_Vehicle_Medical_Unstable_TrackLegSplints = true;
MIRA_Vehicle_Medical_Unstable_TrackLowBP = true;
MIRA_Vehicle_Medical_Unstable_TrackLowHR = true;
MIRA_Vehicle_Medical_Unstable_TrackUnconscious = true;
MIRA_Vehicle_Medical_Vehicles_EnableCar = true;
MIRA_Vehicle_Medical_Vehicles_EnableHelicopter = true;
MIRA_Vehicle_Medical_Vehicles_EnablePlane = true;
MIRA_Vehicle_Medical_Vehicles_EnableShip = true;
MIRA_Vehicle_Medical_Vehicles_EnableTank = true;
MIRA_Vehicle_Medical_VERSION = false;
MIRA_Vehicle_Medical_WarnViewingDead = true;

// ACE Vehicles
force ace_novehicleclanlogo_enabled = false;
ace_vehicles_hideEjectAction = true;
force ace_vehicles_keepEngineRunning = false;
ace_vehicles_speedLimiterStep = 5;
force ace_viewports_enabled = true;

// ACE View Distance Limiter
force force ace_viewdistance_enabled = true;
force ace_viewdistance_limitViewDistance = 12000;
ace_viewdistance_objectViewDistanceCoeff = 0;
ace_viewdistance_viewDistanceAirVehicle = 0;
ace_viewdistance_viewDistanceLandVehicle = 0;
ace_viewdistance_viewDistanceOnFoot = 0;

// ACE View Restriction
force acex_viewrestriction_mode = 0;
force acex_viewrestriction_modeSelectiveAir = 0;
force acex_viewrestriction_modeSelectiveFoot = 0;
force acex_viewrestriction_modeSelectiveLand = 0;
force acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = false;

// ACE Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// ACE Weapons
ace_common_persistentLaserEnabled = false;
force ace_laserpointer_enabled = true;
ace_reload_displayText = true;
ace_reload_showCheckAmmoSelf = false;
ace_weaponselect_displayText = true;

// ACE Weather
force ace_weather_enabled = true;
ace_weather_showCheckAirTemperature = true;
force ace_weather_updateInterval = 60;
force ace_weather_windSimulation = true;

// ACE Wind Deflection
force ace_winddeflection_enabled = false;
force ace_winddeflection_simulationInterval = 0.05;
force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force ace_zeus_autoAddObjects = false;
force ace_zeus_canCreateZeus = 0;
force ace_zeus_radioOrdnance = false;
force ace_zeus_remoteWind = false;
force ace_zeus_revealMines = 0;
force ace_zeus_zeusAscension = true;
force ace_zeus_zeusBird = false;

// AIME Ammo Type Menu
UPSL_aime_change_ammo_setting_ammo_class = true;
UPSL_aime_change_ammo_setting_vehicle_ammo_class = true;

// AIME General
UPSL_aime_setting_hide = true;

// AIME GPS and UAV Terminal
UPSL_aime_uav_terminal_setting_gps_action = true;
UPSL_aime_uav_terminal_setting_term_action = true;
UPSL_aime_uav_terminal_setting_uav_action = true;

// AIME Group Management
UPSL_aime_group_setting_drop_leader_action = true;

// AIME Inventory
UPSL_aime_inventory_setting_assemble_action = true;
UPSL_aime_inventory_setting_backpack_action = true;
UPSL_aime_inventory_setting_holder_action = true;
UPSL_aime_inventory_setting_open_action = true;

// AIME Vehicle Controls
UPSL_aime_vehicle_controls_setting_arty_computer_action = true;
UPSL_aime_vehicle_controls_setting_collision_action = true;
UPSL_aime_vehicle_controls_setting_engine_action = true;
UPSL_aime_vehicle_controls_setting_flaps_action = true;
UPSL_aime_vehicle_controls_setting_gear_action = true;
UPSL_aime_vehicle_controls_setting_hover_action = true;
UPSL_aime_vehicle_controls_setting_lights_action = true;
UPSL_aime_vehicle_controls_setting_manual_action = true;
UPSL_aime_vehicle_controls_setting_user_actions = true;

// AIME Vehicle Seats
UPSL_aime_vehicle_seats_setting_change_action = true;
UPSL_aime_vehicle_seats_setting_force_eject = false;
UPSL_aime_vehicle_seats_setting_getin_action = true;
UPSL_aime_vehicle_seats_setting_getout_action = true;
UPSL_aime_vehicle_seats_setting_turnout_action = true;

// Alternative Running (Client Settings)
AR_EnableDisableAltRunMGBOLT = true;
AR_EnableDisableDoubleShiftPress = false;

// Chemical Warfare
force CBRN_color_contamination0 = [0.9294,0.9843,1];
force CBRN_color_contamination1 = [0.8353,0.9294,0.4627];
force CBRN_color_contamination2 = [1,0.6666,0.6666];
force CBRN_exposureCoughThreshold = 3;
force CBRN_exposureDeathThreshold = 20;
force CBRN_exposureKnockoutThreshold = 10;
force CBRN_exposureLimpThreshold = 7;
force force CBRN_factionMask_BLU_CTRG_F = "";
force force CBRN_factionMask_BLU_F = "";
force force CBRN_factionMask_BLU_G_F = "";
force force CBRN_factionMask_BLU_GEN_F = "";
force force CBRN_factionMask_BLU_T_F = "";
force force CBRN_factionMask_BLU_W_F = "";
force force CBRN_factionMask_CIV_F = "";
force force CBRN_factionMask_CIV_IDAP_F = "";
force force CBRN_factionMask_CSA38_CIV = "";
force force CBRN_factionMask_CSA38_CSA38 = "";
force force CBRN_factionMask_CSA38_CSOB = "";
force force CBRN_factionMask_CSA38_CSSSR = "";
force force CBRN_factionMask_CSA38_FIN = "";
force force CBRN_factionMask_CSA38_GB = "";
force force CBRN_factionMask_CSA38_GERM = "";
force force CBRN_factionMask_CSA38_GERM_DE = "";
force force CBRN_factionMask_CSA38_GERM_FR = "";
force force CBRN_factionMask_CSA38_GERM_LATE = "";
force force CBRN_factionMask_CSA38_GERM_PL = "";
force force CBRN_factionMask_CSA38_GERM_W = "";
force force CBRN_factionMask_CSA38_PL = "";
force force CBRN_factionMask_CSA38_PL2 = "";
force force CBRN_factionMask_CSA38_ROM = "";
force force CBRN_factionMask_CSA38_RU = "";
force force CBRN_factionMask_CSA38_SFK = "";
force force CBRN_factionMask_CSA38_SLOV = "";
force force CBRN_factionMask_CSA38_SPOL = "";
force force CBRN_factionMask_CSA38_SS = "";
force force CBRN_factionMask_CSA38_SS40 = "";
force force CBRN_factionMask_dev_mutants = "";
force force CBRN_factionMask_DSA_DeltaX = "";
force force CBRN_factionMask_DSA_Spooks = "";
force force CBRN_factionMask_Faction_Condor_Legion = "";
force CBRN_factionMask_Faction_IJNAF = "";
force force CBRN_factionMask_fow_aus = "";
force force CBRN_factionMask_fow_heer = "";
force force CBRN_factionMask_fow_hi = "";
force force CBRN_factionMask_fow_ija = "";
force force CBRN_factionMask_fow_ija_nas = "";
force force CBRN_factionMask_fow_it = "";
force force CBRN_factionMask_fow_luftwaffe = "";
force force CBRN_factionMask_fow_uk = "";
force force CBRN_factionMask_fow_uk_faa = "";
force force CBRN_factionMask_fow_uk_raf = "";
force force CBRN_factionMask_fow_usa = "";
force force CBRN_factionMask_fow_usa_navy = "";
force force CBRN_factionMask_fow_usa_p = "";
force force CBRN_factionMask_fow_usmc = "";
force force CBRN_factionMask_fow_waffenss = "";
force force CBRN_factionMask_fow_wehrmacht = "";
force force CBRN_factionMask_GEIST1ArmiaWojskaPolskiego = "";
force force CBRN_factionMask_GEISTAlle = "";
force force CBRN_factionMask_GEISTArmeeBlindeeFr = "";
force force CBRN_factionMask_GEISTArmeeFinlandaise = "";
force force CBRN_factionMask_GEISTArmeeFr = "";
force force CBRN_factionMask_GEISTArmiaKrajowa = "";
force force CBRN_factionMask_GEISTBrArmy = "";
force force CBRN_factionMask_GEISTCivils = "";
force force CBRN_factionMask_GEISTHeer = "";
force force CBRN_factionMask_GEISTKazaki = "";
force force CBRN_factionMask_GEISTLotnictwoWojskaPolskiego = "";
force force CBRN_factionMask_GEISTLuftwaffe = "";
force force CBRN_factionMask_GEISTMiliceFrancaise = "";
force force CBRN_factionMask_GEISTNKVD = "";
force force CBRN_factionMask_GEISTPanzerAufklarung = "";
force force CBRN_factionMask_GEISTPanzerwaffe = "";
force force CBRN_factionMask_GEISTPartizany = "";
force force CBRN_factionMask_GEISTRedArmy = "";
force force CBRN_factionMask_GEISTRedArmyCavalry = "";
force force CBRN_factionMask_GEISTRedArmyTankTroops = "";
force force CBRN_factionMask_GEISTResistance = "";
force force CBRN_factionMask_GEISTROA = "";
force force CBRN_factionMask_GEISTSS = "";
force force CBRN_factionMask_GEISTSturmartillerie = "";
force force CBRN_factionMask_GEISTUSAF = "";
force force CBRN_factionMask_GEISTUSArmored = "";
force force CBRN_factionMask_GEISTUSArmy = "";
force force CBRN_factionMask_GEISTVDV = "";
force force CBRN_factionMask_GEISTVVS = "";
force force CBRN_factionMask_GEISTWaffenSS = "";
force force CBRN_factionMask_GEISTWehrmacht = "";
force force CBRN_factionMask_IND_C_F = "";
force force CBRN_factionMask_IND_E_F = "";
force force CBRN_factionMask_IND_F = "";
force force CBRN_factionMask_IND_G_F = "";
force force CBRN_factionMask_IND_L_F = "";
force force CBRN_factionMask_Interactive_F = "";
force force CBRN_factionMask_LIB_ACI = "";
force force CBRN_factionMask_LIB_ARR = "";
force force CBRN_factionMask_LIB_CIV = "";
force force CBRN_factionMask_LIB_DAK = "";
force force CBRN_factionMask_LIB_FFI = "";
force force CBRN_factionMask_LIB_FSJ = "";
force force CBRN_factionMask_LIB_GUER = "";
force force CBRN_factionMask_LIB_LUFTWAFFE = "";
force force CBRN_factionMask_LIB_LUFTWAFFE_w = "";
force force CBRN_factionMask_LIB_MKHL = "";
force force CBRN_factionMask_LIB_NAC = "";
force force CBRN_factionMask_LIB_NKVD = "";
force force CBRN_factionMask_LIB_PANZERWAFFE = "";
force force CBRN_factionMask_LIB_PANZERWAFFE_w = "";
force force CBRN_factionMask_LIB_RAAF = "";
force force CBRN_factionMask_LIB_RAF = "";
force force CBRN_factionMask_LIB_RBAF = "";
force force CBRN_factionMask_LIB_RKKA = "";
force force CBRN_factionMask_LIB_RKKA_w = "";
force force CBRN_factionMask_LIB_SD_ATTACKERS = "";
force force CBRN_factionMask_LIB_SD_DEFENDERS = "";
force force CBRN_factionMask_LIB_SD_SystemUnits = "";
force force CBRN_factionMask_LIB_UK_AB = "";
force force CBRN_factionMask_LIB_UK_AB_w = "";
force force CBRN_factionMask_LIB_UK_ARMY = "";
force force CBRN_factionMask_LIB_UK_ARMY_w = "";
force force CBRN_factionMask_LIB_UK_DR = "";
force force CBRN_factionMask_LIB_US_101AB = "";
force force CBRN_factionMask_LIB_US_82AB = "";
force force CBRN_factionMask_LIB_US_AIRFORCE = "";
force force CBRN_factionMask_LIB_US_AIRFORCE_w = "";
force force CBRN_factionMask_LIB_US_ARMY = "";
force force CBRN_factionMask_LIB_US_ARMY_w = "";
force force CBRN_factionMask_LIB_US_RANGERS = "";
force force CBRN_factionMask_LIB_US_TANK_TROOPS = "";
force force CBRN_factionMask_LIB_US_TANK_TROOPS_w = "";
force force CBRN_factionMask_LIB_USSR_AIRFORCE = "";
force force CBRN_factionMask_LIB_USSR_AIRFORCE_w = "";
force force CBRN_factionMask_LIB_USSR_TANK_TROOPS = "";
force force CBRN_factionMask_LIB_USSR_TANK_TROOPS_w = "";
force force CBRN_factionMask_LIB_WEHRMACHT = "";
force force CBRN_factionMask_LIB_WEHRMACHT_w = "";
force force CBRN_factionMask_OPF_F = "";
force force CBRN_factionMask_OPF_G_F = "";
force force CBRN_factionMask_OPF_GEN_F = "";
force force CBRN_factionMask_OPF_R_F = "";
force force CBRN_factionMask_OPF_T_F = "";
force force CBRN_factionMask_OPF_V_F = "";
force force CBRN_factionMask_Ryanzombiesfaction = "";
force force CBRN_factionMask_Ryanzombiesfactionmodule = "";
force force CBRN_factionMask_Ryanzombiesfactionopfor = "";
force force CBRN_factionMask_sab_fl_faction_blue = "";
force force CBRN_factionMask_sab_fl_faction_green = "";
force force CBRN_factionMask_sab_fl_faction_red = "";
force force CBRN_factionMask_sab_nl_faction_blue = "";
force force CBRN_factionMask_sab_nl_faction_civ = "";
force force CBRN_factionMask_sab_nl_faction_green = "";
force force CBRN_factionMask_sab_nl_faction_red = "";
force force CBRN_factionMask_sab_sw_faction_blue = "";
force force CBRN_factionMask_sab_sw_faction_green = "";
force force CBRN_factionMask_sab_sw_faction_red = "";
force force CBRN_factionMask_SG_STURM = "";
force force CBRN_factionMask_SG_STURM_w = "";
force force CBRN_factionMask_SG_STURMPANZER = "";
force force CBRN_factionMask_Simc_MC_50 = "";
force force CBRN_factionMask_Simc_MC_53 = "";
force force CBRN_factionMask_Simc_UA_43 = "";
force force CBRN_factionMask_Simc_UA_44 = "";
force force CBRN_factionMask_Simc_UA_50 = "";
force force CBRN_factionMask_Simc_UA_53 = "";
force force CBRN_factionMask_Simc_UA_Erla = "";
force force CBRN_factionMask_Simc_UA_ETO = "";
force force CBRN_factionMask_Simc_UA_ETO_w = "";
force force CBRN_factionMask_Simc_UA_NAC = "";
force force CBRN_factionMask_Simc_UA_PTO = "";
force force CBRN_factionMask_Simc_UA_PTO_erla = "";
force force CBRN_factionMask_Simc_UA_PTO_late = "";
force force CBRN_factionMask_Simc_UAAB_generic = "";
force force CBRN_factionMask_Simc_US_101AB = "";
force force CBRN_factionMask_Simc_US_101AB_Late = "";
force force CBRN_factionMask_Simc_US_17AB = "";
force force CBRN_factionMask_Simc_US_82AB = "";
force force CBRN_factionMask_Simc_US_82AB_Early = "";
force force CBRN_factionMask_Simc_US_82AB_Late = "";
force force CBRN_factionMask_Simc_USMC = "";
force force CBRN_factionMask_Simc_USMC_42 = "";
force force CBRN_factionMask_Simc_USMC_44 = "";
force force CBRN_factionMask_Simc_USMC_45 = "";
force force CBRN_factionMask_Simc_USMC_Green = "";
force force CBRN_factionMask_Simc_USMC_sand = "";
force force CBRN_factionMask_Simc_USN = "";
force force CBRN_factionMask_Virtual_F = "";
force force CBRN_factionMask_WBK_AI = "";
force force CBRN_factionMask_WBK_AI_Melee = "";
force force CBRN_factionMask_WBK_AI_ZHAMBIES = "";

// Community Base Addons
cba_diagnostic_ConsoleIndentType = -1;
force cba_diagnostic_watchInfoRefreshRate = 0.2;
cba_disposable_dropUsedLauncher = 2;
force cba_disposable_replaceDisposableLauncher = true;
cba_events_repetitionMode = 1;
force cba_network_loadoutValidation = 0;
cba_optics_usePipOptics = true;
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// DevourerKing Common
dev_cba_damageMultiplier = 1;
dev_cba_friendly = "[""VirtualCurator_F"","" ""B_VirtualCurator_F"","" ""O_VirtualCurator_F"","" ""I_VirtualCurator_F"","" ""C_VirtualCurator_F""]";
force dev_cba_friendlySide = true;
force dev_cba_killswitch = false;
force dev_zombie_deleteWeapon = true;

// DUI - Squad Radar - Indicators
force diwako_dui_indicators_crew_range_enabled = false;
diwako_dui_indicators_fov_scale = false;
diwako_dui_indicators_icon_buddy = true;
diwako_dui_indicators_icon_leader = true;
diwako_dui_indicators_icon_medic = true;
diwako_dui_indicators_range = 20;
diwako_dui_indicators_range_crew = 300;
diwako_dui_indicators_range_scale = false;
diwako_dui_indicators_show = true;
diwako_dui_indicators_size = 1;
diwako_dui_indicators_style = "standard";
diwako_dui_indicators_useACENametagsRange = true;

// DUI - Squad Radar - Main
diwako_dui_ace_hide_interaction = true;
diwako_dui_colors = "standard";
diwako_dui_font = "RobotoCondensed";
diwako_dui_icon_style = "standard";
diwako_dui_main_hide_dialog = true;
diwako_dui_main_hide_ui_by_default = false;
diwako_dui_main_squadBlue = [0,0,1,1];
diwako_dui_main_squadGreen = [0,1,0,1];
diwako_dui_main_squadMain = [1,1,1,1];
diwako_dui_main_squadRed = [1,0,0,1];
diwako_dui_main_squadYellow = [1,1,0,1];
diwako_dui_main_trackingColor = [0.93,0.26,0.93,1];
diwako_dui_reset_ui_pos = false;

// DUI - Squad Radar - Nametags
diwako_dui_nametags_customRankStyle = "[[""PRIVATE"""",""""CORPORAL"""",""""SERGEANT"""",""""LIEUTENANT"""",""""CAPTAIN"""",""""MAJOR"""",""""COLONEL""],[""Pvt."""",""""Cpl."""",""""Sgt."""",""""Lt."""",""""Capt."""",""""Maj."""",""""Col.""]]";
diwako_dui_nametags_deadColor = [0.2,0.2,0.2,1];
diwako_dui_nametags_deadRenderDistance = 3.5;
diwako_dui_nametags_drawRank = true;
diwako_dui_nametags_enabled = true;
diwako_dui_nametags_enableFOVBoost = true;
diwako_dui_nametags_enableOcclusion = true;
diwako_dui_nametags_fadeInTime = 0.05;
diwako_dui_nametags_fadeOutTime = 0.5;
diwako_dui_nametags_fontGroup = "RobotoCondensedLight";
diwako_dui_nametags_fontGroupNameSize = 8;
diwako_dui_nametags_fontName = "RobotoCondensedBold";
diwako_dui_nametags_fontNameSize = 10;
diwako_dui_nametags_groupColor = [1,1,1,1];
diwako_dui_nametags_groupFontShadow = 1;
diwako_dui_nametags_groupNameOtherGroupColor = [0.6,0.85,0.6,1];
diwako_dui_nametags_nameFontShadow = 1;
diwako_dui_nametags_nameOtherGroupColor = [0.2,1,0,1];
diwako_dui_nametags_rankNameStyle = "default";
diwako_dui_nametags_renderDistance = 40;
diwako_dui_nametags_showUnconAsDead = true;
diwako_dui_nametags_useLIS = true;
diwako_dui_nametags_useSideIsFriendly = true;

// DUI - Squad Radar - Radar
diwako_dui_compass_hide_alone_group = false;
diwako_dui_compass_hide_blip_alone_group = false;
diwako_dui_compass_icon_scale = 1;
diwako_dui_compass_opacity = 1;
diwako_dui_compass_style = ["\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass_limited.paa","\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass.paa"];
diwako_dui_compassRange = 35;
diwako_dui_compassRefreshrate = 0;
diwako_dui_dir_showMildot = false;
diwako_dui_dir_size = 1.25;
diwako_dui_distanceWarning = 3;
diwako_dui_enable_compass = true;
diwako_dui_enable_compass_dir = 1;
diwako_dui_enable_occlusion = false;
diwako_dui_enable_occlusion_cone = 360;
diwako_dui_hudScaling = 1;
diwako_dui_namelist = true;
diwako_dui_namelist_bg = 0;
diwako_dui_namelist_only_buddy_icon = false;
diwako_dui_namelist_size = 1;
diwako_dui_namelist_text_shadow = 2;
diwako_dui_namelist_width = 215;
diwako_dui_radar_ace_finger = true;
force diwako_dui_radar_ace_medic = true;
diwako_dui_radar_compassRangeCrew = 500;
diwako_dui_radar_dir_padding = 25;
diwako_dui_radar_dir_shadow = 2;
diwako_dui_radar_group_by_vehicle = false;
diwako_dui_radar_icon_opacity = 1;
diwako_dui_radar_icon_opacity_no_player = true;
force diwako_dui_radar_icon_priority_setting = 1;
diwako_dui_radar_icon_scale_crew = 6;
diwako_dui_radar_leadingZeroes = false;
diwako_dui_radar_namelist_hideWhenLeader = false;
diwako_dui_radar_namelist_vertical_spacing = 1;
diwako_dui_radar_occlusion_fade_in_time = 1;
diwako_dui_radar_occlusion_fade_time = 10;
diwako_dui_radar_pointer_color = [1,0.5,0,1];
diwako_dui_radar_pointer_style = "standard";
diwako_dui_radar_show_cardinal_points = true;
diwako_dui_radar_showSpeaking = true;
diwako_dui_radar_showSpeaking_radioOnly = false;
diwako_dui_radar_showSpeaking_replaceIcon = true;
force diwako_dui_radar_sortType = "none";
force diwako_dui_radar_sqlFirst = false;
force diwako_dui_radar_syncGroup = false;
force diwako_dui_radar_vehicleCompassEnabled = false;
diwako_dui_use_layout_editor = false;

// Fire support PLUS
force FSPLUS_105mmTrainingShell = true;
force FSPLUS_122Rocket = true;
force FSPLUS_122RocketBarrage = true;
force FSPLUS_155Barrage = true;
force FSPLUS_230HE = true;
force FSPLUS_230HEBarrage = true;
force FSPLUS_230mmAP = true;
force FSPLUS_230mmFletchette = true;
force FSPLUS_230mmHeatSeeking = true;
force FSPLUS_230mmMine = true;
force FSPLUS_230mmTrainingRocket = true;
force FSPLUS_82mmTrainingShell = true;
force FSPLUS_82MortarBarrage = true;
force FSPLUS_Big_Nuke = true;
force FSPLUS_CruiseMissile = true;
force FSPLUS_CW_AsphyxiantGas = true;
force FSPLUS_CW_CSGas = true;
force FSPLUS_CW_NerveGas = true;
force FSPLUS_CW_RemoveExposure = true;
force FSPLUS_EMP = true;
force FSPLUS_JDAM = true;
force FSPLUS_Napalm = true;
force FSPLUS_Not230mmBarrage = true;
force FSPLUS_RodsfromGod = true;
force FSPLUS_Smoke_white = true;
force FSPLUS_Variable_Nuke = true;

// Freestyle's Crash Landing
force force fscl_captiveSystem = false;
force force fscl_damageTreshold = 99;
force force fscl_debug = false;
force force fscl_ejectionProp = 0;
force force fscl_ejectionSystem = false;
force force fscl_gForceThreshold = 5;
force force fscl_ignoreNonPlayerVehicles = true;
force force fscl_stateThreshold = 10;

// Freestyle's Nukes
force force FSN_AI_Nuke_Bias = "STANDARD";
FSN_AI_Nuke_maxColleteral = 10;
FSN_AI_Nuke_maxFriendly = 10;
FSN_AI_Nuke_maxRatioColleteralEnemy = 1;
FSN_AI_Nuke_maxRatioFriendlyEnemy = 1;
FSN_AI_Nuke_minEnemy = 10;
FSN_AI_Nuke_NonEnemyEnemy = 1;
FSN_AI_Nuke_order = 0;
force force FSN_Auto_Nuking = false;
force FSN_Debug = false;
force force FSN_Fallout = true;

// GRAD Trenches
force grad_trenches_functions_allowBigEnvelope = true;
force grad_trenches_functions_allowCamouflage = true;
force grad_trenches_functions_allowDigging = true;
force grad_trenches_functions_allowEffects = true;
force grad_trenches_functions_allowGiantEnvelope = true;
force grad_trenches_functions_allowHitDecay = true;
force grad_trenches_functions_allowLongEnvelope = true;
force grad_trenches_functions_allowShortEnvelope = true;
force grad_trenches_functions_allowSmallEnvelope = true;
force grad_trenches_functions_allowTrenchDecay = false;
force grad_trenches_functions_allowVehicleEnvelope = true;
force grad_trenches_functions_bigEnvelopeDamageMultiplier = 2;
force grad_trenches_functions_bigEnvelopeDigTime = 40;
force grad_trenches_functions_bigEnvelopeRemovalTime = -1;
force grad_trenches_functions_buildFatigueFactor = 0;
force grad_trenches_functions_camouflageRequireEntrenchmentTool = true;
force grad_trenches_functions_createTrenchMarker = false;
force grad_trenches_functions_decayTime = 1800;
force grad_trenches_functions_giantEnvelopeDamageMultiplier = 1;
force grad_trenches_functions_giantEnvelopeDigTime = 90;
force grad_trenches_functions_giantEnvelopeRemovalTime = -1;
force grad_trenches_functions_hitDecayMultiplier = 0.334378;
force grad_trenches_functions_LongEnvelopeDigTime = 100;
force grad_trenches_functions_LongEnvelopeRemovalTime = -1;
force grad_trenches_functions_shortEnvelopeDamageMultiplier = 2;
force grad_trenches_functions_shortEnvelopeDigTime = 15;
force grad_trenches_functions_shortEnvelopeRemovalTime = -1;
force grad_trenches_functions_smallEnvelopeDamageMultiplier = 3;
force grad_trenches_functions_smallEnvelopeDigTime = 30;
force grad_trenches_functions_smallEnvelopeRemovalTime = -1;
force grad_trenches_functions_stopBuildingAtFatigueMax = false;
force grad_trenches_functions_timeoutToDecay = 7200;
force grad_trenches_functions_vehicleEnvelopeDamageMultiplier = 1;
force grad_trenches_functions_vehicleEnvelopeDigTime = 120;
force grad_trenches_functions_vehicleEnvelopeRemovalTime = -1;

// Improved Melee System (Client Settings)
force force IMS_CustomCamer_Y = 3;
force force IMS_CustomCameraUsedByUserAllowed = true;
force force IMS_EnablePlayerSounds = true;
force force IMS_HudCoordinate_X = 0.01;
force force IMS_HudCoordinate_Y = 0.9;
force force IMS_ShowHealthHud = true;

// Improved Melee System (Server Settings)
force force IMS_AddKnifeToUnit = false;
force force IMS_BayonetDistance = "6";
force force IMS_BayonetOnAI = true;
force force IMS_BluntWeapon = false;
force force IMS_CustomAIHEALTH = "2";
force force IMS_DamageMultiplierParam = "1";
force IMS_DamageMultiplierParamPlayer = "1";
force force IMS_ExecutionChanceParametr = "20";
force IMS_isFistsAllowd = true;
force force IMS_isHumansCanHitSM = false;
force force IMS_isImsCanHitAllies = true;
force force IMS_isKickButtInstaKill = false;
force force IMS_isStaticDeaths = false;
force force IMS_RifleDodgeSet = false;
force force IMS_StealthAI_Ears = 15;
force force IMS_StealthAI_Eyes = 40;
force force IMS_WBK_CUSTOMCAMSERVER = false;
force IMS_WBK_MAINFPTP = true;

// KKA3 TFAR Animations
kka3_animationMode = "kka3_radio01";
kka3_disabled = false;

// LAMBS Danger
force lambs_danger_cqbRange = 60;
lambs_danger_disableAIAutonomousManoeuvres = false;
lambs_danger_disableAIDeployStaticWeapons = false;
lambs_danger_disableAIFindStaticWeapons = false;
lambs_danger_disableAIHideFromTanksAndAircraft = false;
lambs_danger_disableAIPlayerGroup = false;
lambs_danger_disableAIPlayerGroupReaction = false;
lambs_danger_disableAutonomousFlares = false;
lambs_danger_disableAutonomousSmokeGrenades = false;
force lambs_danger_panicChance = 0.1;

// LAMBS Danger Eventhandlers
force lambs_eventhandlers_ExplosionEventHandlerEnabled = true;
force lambs_eventhandlers_ExplosionReactionTime = 9;

// LAMBS Danger WP
force lambs_wp_autoAddArtillery = false;

// LAMBS Main
force lambs_main_combatShareRange = 200;
force lambs_main_debug_drawAllUnitsInVehicles = false;
force lambs_main_debug_Drawing = false;
force lambs_main_debug_FSM = false;
force lambs_main_debug_FSM_civ = false;
force lambs_main_debug_functions = false;
force lambs_main_debug_RenderExpectedDestination = false;
lambs_main_disableAICallouts = false;
lambs_main_disableAIDodge = false;
lambs_main_disableAIFleeing = false;
lambs_main_disableAIGestures = false;
lambs_main_disableAutonomousMunitionSwitching = false;
lambs_main_disablePlayerGroupSuppression = false;
force lambs_main_indoorMove = 0.1;
force lambs_main_maxRevealValue = 1;
force lambs_main_minFriendlySuppressionDistance = 5;
force lambs_main_minObstacleProximity = 5;
force lambs_main_minSuppressionRange = 50;
force lambs_main_radioBackpack = 2000;
lambs_main_radioDisabled = false;
force lambs_main_radioEast = 500;
force lambs_main_radioGuer = 500;
force lambs_main_radioShout = 100;
force lambs_main_radioWest = 500;

// Necroplague
force dev_cba_infection = true;
dev_cba_infection_prolongTime = 900;
dev_cba_infection_resurrectAmugusChance = 0;
dev_cba_infection_resurrectParasiteChance = 0.1;
dev_cba_infection_resurrectTime = "[25,30,35]";
force dev_cba_infection_resurrectWebknight = false;
force dev_cba_infection_resurrectZombie = true;
dev_cba_infection_totalTime = 180;

// Necroplague - Infected
dev_zombie_attack_damageMn = 0.125;
dev_zombie_attack_damageVeh = 0.001;
dev_zombie_attack_launchVeh = "[0,1,1]";
dev_zombie_attack_reachMan = 3.5;
dev_zombie_attack_reachVeh = 8;
dev_zombie_attack_timeout = 0.5;
dev_zombie_distance_agro = 50;
dev_zombie_distance_hunt = 100;
dev_zombie_distance_max = 500;
dev_zombie_distance_roam = 75;
force dev_zombie_greenEyes = true;
dev_zombie_head_caliber = 9;
dev_zombie_head_chance = 0.5;
dev_zombie_health = 0.5;
dev_zombie_infectionChance = 0.5;
force dev_zombie_uniformFix = false;
force dev_zombie_useGlasses = false;
force dev_zombie_useIdentity = true;

// Necroplague - Spitter
dev_toxmut_attack_damageMan = 0.3;
dev_toxmut_attack_damageManSpit = 0.5;
dev_toxmut_attack_damageVeh = 0.01;
dev_toxmut_attack_launchVeh = "[0,1,0.1]";
dev_toxmut_attack_reachMan = 3.5;
dev_toxmut_attack_reachSpit = 30;
dev_toxmut_attack_reachVeh = 8;
dev_toxmut_attack_specialChance = 0.5;
dev_toxmut_attack_timeout = 0.5;
dev_toxmut_attack_timeoutSpit = 15;
dev_toxmut_distance_agro = 75;
dev_toxmut_distance_hunt = 300;
dev_toxmut_distance_max = 1000;
dev_toxmut_distance_roam = 75;
dev_toxmut_health = 1;

// Necroplague - Stalker
dev_form939_attack_damageMan = 0.5;
dev_form939_attack_damageVeh = 0.01;
dev_form939_attack_launchVeh = "[0,1,1]";
dev_form939_attack_psychChance = 0.1;
dev_form939_attack_psychDuration = 15;
dev_form939_attack_psychTimeout = 30;
dev_form939_attack_reachMan = 5;
dev_form939_attack_reachVeh = 8;
dev_form939_attack_timeout = 0.5;
dev_form939_distance_agro = 100;
dev_form939_distance_hunt = 1400;
dev_form939_distance_max = 1500;
dev_form939_distance_roam = 75;
dev_form939_health = 0.11;

// Necroplague - The Bully
dev_asymhuman_attack_bigArmChance = 0.5;
dev_asymhuman_attack_damageMan = 0.5;
dev_asymhuman_attack_damageVeh = 0.2;
dev_asymhuman_attack_infectionChance = 0.5;
dev_asymhuman_attack_launchMan = "[0,6,5]";
dev_asymhuman_attack_launchVeh = "[0,10,5]";
dev_asymhuman_attack_reachMan = 3.5;
dev_asymhuman_attack_reachVeh = 8;
dev_asymhuman_attack_timeout = 0.5;
dev_asymhuman_distance_agro = 75;
dev_asymhuman_distance_hunt = 300;
dev_asymhuman_distance_max = 1000;
dev_asymhuman_distance_roam = 75;
dev_asymhuman_health = 1;

// Necroplague - The Hivemind
dev_hivemind_hallucinate2Distance = 50;
dev_hivemind_hallucinateProbabilityClose = 20;
dev_hivemind_hallucinateProbabilityFar = 20;
dev_hivemind_health = 0.2;
dev_hivemind_maxCooldown = 40;
dev_hivemind_maxDistance = 150;
dev_hivemind_minCooldown = 15;
dev_hivemind_suicideTime = 180;

// Necroplague - The Jumper
dev_asymhuman_stage2_agroDistance = 50;
dev_asymhuman_stage2_attack2Timeout = 10;
dev_asymhuman_stage2_attackDistanceMan = 2.5;
dev_asymhuman_stage2_attackDistanceVeh = 8;
dev_asymhuman_stage2_attackTimeout = 1;
dev_asymhuman_stage2_damageMan = 0.2;
dev_asymhuman_stage2_damageManAttack2 = 0.3;
dev_asymhuman_stage2_damageVeh = 0.1;
dev_asymhuman_stage2_distance_roam = 50;
dev_asymhuman_stage2_health = 0.1;
dev_asymhuman_stage2_huntDistance = 75;
dev_asymhuman_stage2_infectionChance = 0.5;
dev_asymhuman_stage2_jumpTimeout = 30;
dev_asymhuman_stage2_maxDistance = 500;

// Necroplague - The Parasite
dev_parasite_agroDistance = 1000;
dev_parasite_attack2Timeout = 4;
dev_parasite_attack3Timeout = 20;
dev_parasite_attackDistanceMan = 3.5;
dev_parasite_attackDistanceVeh = 7;
dev_parasite_attackTimeout = 1;
dev_parasite_damageMan = 0.2;
dev_parasite_damageManAttack2 = 0.2;
dev_parasite_damageManAttack3 = 0.25;
dev_parasite_damageVeh = 0.001;
dev_parasite_distance_roam = 75;
dev_parasite_health = 0.05;
dev_parasite_huntDistance = 1500;
dev_parasite_infectionChance = 1;
dev_parasite_jumpTimeout = 20;
dev_parasite_maxDistance = 2000;
dev_parasite_specialChance = 0.1;

// TFAR - Clientside settings
TFAR_curatorCamEars = false;
TFAR_default_radioVolume = 6;
TFAR_intercomDucking = 0.2;
TFAR_intercomVolume = 0.1;
TFAR_moveWhileTabbedOut = false;
TFAR_noAutomoveSpectator = false;
TFAR_oldVolumeHint = false;
TFAR_pluginTimeout = 4;
TFAR_PosUpdateMode = 0.1;
TFAR_showChannelChangedHint = true;
TFAR_ShowDiaryRecord = true;
TFAR_showTransmittingHint = true;
TFAR_ShowVolumeHUD = false;
TFAR_tangentReleaseDelay = 0;
TFAR_VolumeHudTransparency = 0;
TFAR_volumeModifier_forceSpeech = false;

// TFAR - Global settings
force TFAR_AICanHearPlayer = true;
force TFAR_AICanHearSpeaker = false;
force TFAR_allowDebugging = true;
tfar_core_noTSNotConnectedHint = false;
force TFAR_defaultIntercomSlot = 0;
force TFAR_disableAutoMute = false;
force TFAR_enableIntercom = true;
force TFAR_experimentalVehicleIsolation = true;
force TFAR_fullDuplex = true;
force TFAR_giveLongRangeRadioToGroupLeaders = false;
force TFAR_giveMicroDagrToSoldier = true;
force TFAR_givePersonalRadioToRegularSoldier = false;
force TFAR_globalRadioRangeCoef = 1;
force TFAR_instantiate_instantiateAtBriefing = false;
force TFAR_objectInterceptionEnabled = false;
force TFAR_objectInterceptionStrength = 0;
force tfar_radiocode_east = "_opfor";
force tfar_radiocode_independent = "_guer";
force tfar_radiocode_west = "_bluefor";
force tfar_radioCodesDisabled = true;
force TFAR_SameLRFrequenciesForSide = false;
force TFAR_SameSRFrequenciesForSide = false;
force TFAR_setting_defaultFrequencies_lr_east = "";
force TFAR_setting_defaultFrequencies_lr_independent = "60";
force TFAR_setting_defaultFrequencies_lr_west = "60";
force TFAR_setting_defaultFrequencies_sr_east = "";
force TFAR_setting_defaultFrequencies_sr_independent = "";
force TFAR_setting_defaultFrequencies_sr_west = "123";
force TFAR_setting_DefaultRadio_Airborne_east = "TFAR_mr6000l";
force TFAR_setting_DefaultRadio_Airborne_Independent = "TFAR_anarc164";
force TFAR_setting_DefaultRadio_Airborne_West = "TFAR_anarc210";
force TFAR_setting_DefaultRadio_Backpack_east = "TFAR_mr3000";
force TFAR_setting_DefaultRadio_Backpack_Independent = "TFAR_anprc155";
force TFAR_setting_DefaultRadio_Backpack_west = "TFAR_rt1523g";
force TFAR_setting_DefaultRadio_Personal_east = "TFAR_fadak";
force TFAR_setting_DefaultRadio_Personal_Independent = "TFAR_anprc148jem";
force TFAR_setting_DefaultRadio_Personal_West = "TFAR_anprc152";
force TFAR_setting_DefaultRadio_Rifleman_East = "TFAR_pnr1000a";
force TFAR_setting_DefaultRadio_Rifleman_Independent = "TFAR_anprc154";
force TFAR_setting_DefaultRadio_Rifleman_West = "TFAR_rf7800str";
force TFAR_spectatorCanHearEnemyUnits = true;
force TFAR_spectatorCanHearFriendlies = true;
force TFAR_takingRadio = 2;
force TFAR_Teamspeak_Channel_Password = "123";
force tfar_terrain_interception_coefficient = 0;
force TFAR_voiceCone = true;

// WebKnight's Zombies
force force WBK_ZombiesIsUseBitingAnimation = true;
force force WBK_ZombiesIsUseParticleDeathControl = true;
force force WBK_ZombiesIsUseStatDeathControl = false;
force WBK_ZommbiesLeaperHealthParam = "120";
force WBK_ZommbiesMeleeHealthParam = "100";
force WBK_ZommbiesSmasherHealthParam = "3500";
force WBK_ZommbiesSmasherJumpParam = true;
force WBK_ZommbiesSmasherThrowParam = true;

// ZaD - ACE integration
force force zad_ace_int_pillHeal_enable = true;

// Zeus Enhanced
zen_camera_adaptiveSpeed = true;
zen_camera_defaultSpeedCoef = 1;
zen_camera_fastSpeedCoef = 1;
zen_camera_followTerrain = true;
force force zen_common_ascensionMessages = true;
force zen_common_autoAddObjects = true;
force zen_common_cameraBird = false;
zen_common_darkMode = false;
zen_common_disableGearAnim = false;
zen_common_preferredArsenal = 1;
force force zen_compat_ace_hideModules = false;
zen_context_menu_enabled = 2;
zen_context_menu_overrideWaypoints = false;
zen_editor_addGroupIcons = false;
zen_editor_declutterEmptyTree = true;
zen_editor_disableLiveSearch = false;
zen_editor_moveDisplayToEdge = true;
force zen_editor_parachuteSounds = true;
zen_editor_previews_enabled = true;
zen_editor_randomizeCopyPaste = false;
zen_editor_removeWatermark = true;
zen_editor_unitRadioMessages = 0;
force force zen_placement_enabled = true;
zen_remote_control_cameraExitPosition = 2;
zen_visibility_enabled = false;
zen_vision_enableBlackHot = false;
zen_vision_enableBlackHotGreenCold = false;
zen_vision_enableBlackHotRedCold = false;
zen_vision_enableGreenHotCold = false;
zen_vision_enableNVG = true;
zen_vision_enableRedGreenThermal = false;
zen_vision_enableRedHotCold = false;
zen_vision_enableWhiteHot = true;
zen_vision_enableWhiteHotRedCold = false;

// Zeus Enhanced - Faction Filter
force force zen_faction_filter_0_CSA38_PL = true;
force force zen_faction_filter_0_CSA38_RU = true;
zen_faction_filter_0_dev_groups_east = true;
zen_faction_filter_0_dev_mutants = true;
force force zen_faction_filter_0_GEIST1ArmiaWojskaPolskiego = false;
force force zen_faction_filter_0_GEISTNKVD = false;
force force zen_faction_filter_0_GEISTPartizany = false;
force force zen_faction_filter_0_GEISTRedArmy = false;
force force zen_faction_filter_0_GEISTRedArmyTankTroops = false;
force force zen_faction_filter_0_GEISTVDV = false;
force force zen_faction_filter_0_GEISTVVS = false;
force force zen_faction_filter_0_LIB_NKVD = true;
force force zen_faction_filter_0_LIB_RKKA = true;
force force zen_faction_filter_0_LIB_RKKA_w = true;
force force zen_faction_filter_0_LIB_USSR_AIRFORCE = true;
force force zen_faction_filter_0_LIB_USSR_AIRFORCE_w = true;
force force zen_faction_filter_0_LIB_USSR_TANK_TROOPS = true;
zen_faction_filter_0_OPF_F = true;
zen_faction_filter_0_OPF_G_F = true;
zen_faction_filter_0_OPF_GEN_F = true;
zen_faction_filter_0_OPF_R_F = true;
zen_faction_filter_0_OPF_T_F = true;
force force zen_faction_filter_0_Ryanzombiesfactionopfor = true;
force force zen_faction_filter_0_sab_fl_faction_red = true;
force force zen_faction_filter_0_sab_sw_faction_red = true;
force force zen_faction_filter_0_WBK_AI = true;
force force zen_faction_filter_0_WBK_AI_Melee = true;
force force zen_faction_filter_0_WBK_AI_ZHAMBIES = true;
zen_faction_filter_1_BLU_CTRG_F = true;
zen_faction_filter_1_BLU_F = true;
zen_faction_filter_1_BLU_G_F = true;
zen_faction_filter_1_BLU_GEN_F = true;
zen_faction_filter_1_BLU_T_F = true;
zen_faction_filter_1_BLU_W_F = true;
force force zen_faction_filter_1_CSA38_GERM = true;
force force zen_faction_filter_1_CSA38_GERM_DE = true;
force force zen_faction_filter_1_CSA38_GERM_FR = true;
force force zen_faction_filter_1_CSA38_GERM_LATE = true;
force force zen_faction_filter_1_CSA38_GERM_PL = true;
force force zen_faction_filter_1_CSA38_GERM_W = true;
force force zen_faction_filter_1_CSA38_GERMGROUPS = true;
force force zen_faction_filter_1_CSA38_GERMGROUPS40 = true;
force force zen_faction_filter_1_CSA38_SFK = true;
force force zen_faction_filter_1_CSA38_SS = true;
force force zen_faction_filter_1_CSA38_SS40 = true;
force force zen_faction_filter_1_Default = true;
zen_faction_filter_1_dev_groups_west = true;
zen_faction_filter_1_dev_mutants = true;
force force zen_faction_filter_1_DSA_DeltaX = false;
force force zen_faction_filter_1_DSA_DeltaX_groups = false;
force force zen_faction_filter_1_Faction_Condor_Legion = true;
zen_faction_filter_1_Faction_IJNAF = true;
force force zen_faction_filter_1_fow_heer = true;
force force zen_faction_filter_1_fow_ija = true;
force force zen_faction_filter_1_fow_ija_nas = true;
force force zen_faction_filter_1_fow_luftwaffe = true;
force force zen_faction_filter_1_fow_waffenss = true;
force force zen_faction_filter_1_fow_wehrmacht = true;
force force zen_faction_filter_1_GEISTArmeeFinlandaise = false;
force force zen_faction_filter_1_GEISTHeer = false;
force force zen_faction_filter_1_GEISTKazaki = false;
force force zen_faction_filter_1_GEISTLuftwaffe = false;
force force zen_faction_filter_1_GEISTMiliceFrancaise = false;
force force zen_faction_filter_1_GEISTPanzerAufklarung = false;
force force zen_faction_filter_1_GEISTPanzerwaffe = false;
force force zen_faction_filter_1_GEISTROA = false;
force force zen_faction_filter_1_GEISTSS = false;
force force zen_faction_filter_1_GEISTSturmartillerie = false;
force force zen_faction_filter_1_GEISTWaffenSS = false;
force force zen_faction_filter_1_GEISTWehrmacht = true;
force force zen_faction_filter_1_LIB_ARR = true;
force force zen_faction_filter_1_LIB_DAK = true;
force force zen_faction_filter_1_LIB_FSJ = true;
force force zen_faction_filter_1_LIB_FSJ_Battle = true;
force force zen_faction_filter_1_LIB_FSJ_DAK = true;
force force zen_faction_filter_1_LIB_FSJ_Jump = true;
force force zen_faction_filter_1_LIB_LUFTWAFFE = true;
force force zen_faction_filter_1_LIB_LUFTWAFFE_w = true;
force force zen_faction_filter_1_LIB_MKHL = true;
force force zen_faction_filter_1_LIB_PANZERWAFFE = true;
force force zen_faction_filter_1_LIB_PANZERWAFFE_w = true;
force force zen_faction_filter_1_LIB_RBAF = true;
force force zen_faction_filter_1_LIB_WEHRMACHT = true;
force force zen_faction_filter_1_LIB_WEHRMACHT_w = true;
force force zen_faction_filter_1_LNRD_Luft = true;
force force zen_faction_filter_1_sab_fl_faction_blue = true;
force force zen_faction_filter_1_sab_nl_faction_blue = true;
force force zen_faction_filter_1_sab_sw_faction_blue = true;
force force zen_faction_filter_1_SG_STURM = true;
force force zen_faction_filter_1_SG_STURMPANZER = true;
force force zen_faction_filter_1_WBK_AI = true;
force force zen_faction_filter_1_WBK_AI_Melee = true;
force force zen_faction_filter_1_WBK_AI_ZHAMBIES = true;
force force zen_faction_filter_2_101AB_category = true;
force force zen_faction_filter_2_101AB_mid_category = true;
force force zen_faction_filter_2_101AB_Spat_category = true;
force force zen_faction_filter_2_17AB_Spat_category = true;
force force zen_faction_filter_2_82AB_category = true;
force force zen_faction_filter_2_82AB_early_category = true;
force force zen_faction_filter_2_82AB_mid_category = true;
force force zen_faction_filter_2_82AB_Spat_category = true;
force force zen_faction_filter_2_CSA38_CSA38 = true;
force force zen_faction_filter_2_CSA38_CSOB = true;
force force zen_faction_filter_2_CSA38_CSSSR = true;
force force zen_faction_filter_2_CSA38_CZECHGROUPS = true;
force force zen_faction_filter_2_CSA38_FIN = true;
force force zen_faction_filter_2_CSA38_GB = true;
force force zen_faction_filter_2_CSA38_PL = true;
force force zen_faction_filter_2_CSA38_ROM = true;
force force zen_faction_filter_2_CSA38_SLOV = true;
force force zen_faction_filter_2_CSA38_SPOL = true;
zen_faction_filter_2_dev_groups_indep = true;
zen_faction_filter_2_dev_mutants = true;
force force zen_faction_filter_2_fow_aus = true;
force force zen_faction_filter_2_fow_aus_groups = true;
force force zen_faction_filter_2_fow_hi = true;
force force zen_faction_filter_2_fow_uk = true;
force force zen_faction_filter_2_fow_uk_faa = true;
force force zen_faction_filter_2_fow_usa = true;
force force zen_faction_filter_2_fow_usa_navy = true;
force force zen_faction_filter_2_fow_usa_p = true;
force force zen_faction_filter_2_fow_usmc = true;
force force zen_faction_filter_2_fow_usmc_groups = true;
force force zen_faction_filter_2_GEISTArmeeBlindeeFr = true;
force force zen_faction_filter_2_GEISTArmeeFr = true;
force force zen_faction_filter_2_GEISTArmiaKrajowa = true;
force force zen_faction_filter_2_GEISTBrArmy = true;
force force zen_faction_filter_2_GEISTResistance = true;
force force zen_faction_filter_2_GEISTUSAF = false;
force force zen_faction_filter_2_GEISTUSArmored = false;
force force zen_faction_filter_2_GEISTUSArmy = false;
zen_faction_filter_2_IND_C_F = true;
zen_faction_filter_2_IND_E_F = true;
zen_faction_filter_2_IND_F = true;
zen_faction_filter_2_IND_G_F = true;
zen_faction_filter_2_IND_L_F = true;
force force zen_faction_filter_2_LIB_ACI = true;
force force zen_faction_filter_2_LIB_FFI = true;
force force zen_faction_filter_2_LIB_GUER = true;
force force zen_faction_filter_2_LIB_NAC = true;
force force zen_faction_filter_2_LIB_RAAF = true;
force force zen_faction_filter_2_LIB_RAF = true;
force force zen_faction_filter_2_LIB_UK_AB = true;
force force zen_faction_filter_2_LIB_UK_AB_w = true;
force force zen_faction_filter_2_LIB_UK_ARMY = true;
force force zen_faction_filter_2_LIB_UK_ARMY_w = true;
force force zen_faction_filter_2_LIB_UK_DR = true;
force force zen_faction_filter_2_LIB_US_AIRFORCE = true;
force force zen_faction_filter_2_LIB_US_ARMY = true;
force force zen_faction_filter_2_LIB_US_ARMY_w = true;
force force zen_faction_filter_2_LIB_US_RANGERS = true;
force force zen_faction_filter_2_LIB_US_TANK_TROOPS = true;
force force zen_faction_filter_2_Ryanzombiesfaction = true;
force force zen_faction_filter_2_sab_fl_faction_green = true;
force force zen_faction_filter_2_sab_nl_faction_green = true;
force force zen_faction_filter_2_sab_sw_faction_green = true;
force force zen_faction_filter_2_Simc_UA_43 = true;
force force zen_faction_filter_2_Simc_UA_44 = true;
force force zen_faction_filter_2_Simc_UA_Erla = true;
force force zen_faction_filter_2_Simc_UA_ETO = true;
force force zen_faction_filter_2_Simc_UA_ETO_w = true;
force force zen_faction_filter_2_Simc_UA_NAC = true;
force force zen_faction_filter_2_Simc_UA_PTO = true;
force force zen_faction_filter_2_Simc_UA_PTO_erla = true;
force force zen_faction_filter_2_Simc_UA_PTO_late = true;
force force zen_faction_filter_2_simc_US_1ID = true;
force force zen_faction_filter_2_simc_US_28ID = true;
force force zen_faction_filter_2_simc_US_29ID = true;
force force zen_faction_filter_2_Simc_US_2AD = true;
force force zen_faction_filter_2_simc_US_34ID = true;
force force zen_faction_filter_2_simc_US_36ID = true;
force force zen_faction_filter_2_Simc_US_3ID = true;
force force zen_faction_filter_2_simc_US_4ID = true;
force force zen_faction_filter_2_Simc_US_77ID = true;
force force zen_faction_filter_2_Simc_US_79ID = true;
force force zen_faction_filter_2_Simc_US_7ID = true;
force force zen_faction_filter_2_simc_US_87ID = true;
force force zen_faction_filter_2_simc_US_90ID = true;
force force zen_faction_filter_2_Simc_US_92ID = true;
force force zen_faction_filter_2_Simc_US_9ID = true;
force force zen_faction_filter_2_Simc_US_M42_101AB = true;
force force zen_faction_filter_2_Simc_US_M43_101AB = true;
force force zen_faction_filter_2_Simc_US_M43_17AB = true;
force force zen_faction_filter_2_Simc_US_M43_82AB = true;
force force zen_faction_filter_2_Simc_US_NAC = true;
force force zen_faction_filter_2_simc_US_RGR2 = true;
force force zen_faction_filter_2_Simc_USMC_42 = true;
force force zen_faction_filter_2_Simc_USMC_44 = true;
force force zen_faction_filter_2_Simc_USMC_45 = true;
force force zen_faction_filter_2_Simc_USMC_Green = true;
force force zen_faction_filter_2_Simc_USMC_sand = true;
force force zen_faction_filter_2_Simc_USMC_sand_groups = true;
force force zen_faction_filter_2_Simc_USN = true;
force force zen_faction_filter_2_UAAB_generic_category = true;
force force zen_faction_filter_2_WBK_AI_ZHAMBIES = true;
zen_faction_filter_3_CIV_F = true;
zen_faction_filter_3_CIV_IDAP_F = true;
force force zen_faction_filter_3_CSA38_CIV = true;
zen_faction_filter_3_dev_mutants = true;
force force zen_faction_filter_3_DSA_Spooks = true;
zen_faction_filter_3_EdCat_jbad_vehicles = true;
force force zen_faction_filter_3_GEISTCivils = true;
zen_faction_filter_3_IND_L_F = true;
force force zen_faction_filter_3_LIB_CIV = true;
force force zen_faction_filter_3_sab_nl_faction_civ = true;
